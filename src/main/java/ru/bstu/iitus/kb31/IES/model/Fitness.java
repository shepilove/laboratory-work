package ru.bstu.iitus.kb31.IES.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Scanner;

@EqualsAndHashCode(callSuper = true)
@Data
public class Fitness extends SportsEquipment {
    int weight;

    @Override
    public void init(Scanner scanner) {
    }
}
