package ru.bstu.iitus.kb31.IES.utility;

import lombok.extern.slf4j.Slf4j;
import ru.bstu.iitus.kb31.IES.enums.EquipmentTypeEnum;
import ru.bstu.iitus.kb31.IES.enums.SportTypeEnum;
import ru.bstu.iitus.kb31.IES.exceptions.InvalidInputException;
import ru.bstu.iitus.kb31.IES.exceptions.NoEquipmentException;
import ru.bstu.iitus.kb31.IES.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Slf4j
public class Utility {
    private Scanner scanner = new Scanner(System.in);

    public List<SportsEquipment> putToCollection(Scanner scanner) throws InvalidInputException {
        log.info("Введите количество предметов инвентаря:");
        List<SportsEquipment> equipment = new ArrayList<SportsEquipment>();
        int il = scanner.nextInt();
        if (il <= 0) {
            throw new InvalidInputException("Колличество предметов не может быть отрицательным, ну вы и сами знаете;)");
        } else {
            for (int i = 1; i <= il; i++) {
                try {
                    log.info("Инвентарь № " + i + " Введите тип инвентаря( 1 - мяч, 2 - ракетка, 3 - тренажерный, 4 - копье):");
                    int type = scanner.nextInt();
                    if (type <= 0 || type > 4) {
                        throw new InvalidInputException("Введен неверный тип инвентаря");
                    }
                    if (type == EquipmentTypeEnum.BALL.getKey()) {
                        log.info("Выберите тип мяча (5 - теннисный, 6 - воллейбольный)");
                        int ball = scanner.nextInt();
                        if (ball != 5 && ball != 6) {
                            throw new InvalidInputException("Введен неверный тип мяча");
                        }
                        if (ball == EquipmentTypeEnum.TENNISBALL.getKey()) {
                            TennisBall tennisBall = new TennisBall();
                            tennisBall.init(scanner);
                            equipment.add(tennisBall);
                        }
                        if(ball == EquipmentTypeEnum.VOLLEYBALL.getKey()){
                            VolleyBall volleyBall = new VolleyBall();
                            volleyBall.init(scanner);
                            equipment.add(volleyBall);
                        }
                    }
                    if (type == EquipmentTypeEnum.RACQUET.getKey()) {
                        Racquet racquet = new Racquet();
                        racquet.init(scanner);
                        equipment.add(racquet);
                    }
                    if (type == EquipmentTypeEnum.FITNESS.getKey()) {
                        log.info("Выберите тип оборудования (7 - штанга, 8 - гиря): ");
                        int gum = scanner.nextInt();
                        if (gum != 7 && gum != 8) {
                            throw new InvalidInputException("Введен неверный параметр");
                        }
                        if (gum == EquipmentTypeEnum.BARBELL.getKey()) {
                            Barbell barbell = new Barbell();
                            barbell.init(scanner);
                            equipment.add(barbell);
                        }
                        if (gum == EquipmentTypeEnum.WEIGHT.getKey()) {
                            Weight weight = new Weight();
                            weight.init(scanner);
                            equipment.add(weight);
                        }
                    }
                    if (type == EquipmentTypeEnum.TROWINGSPEAR.getKey()) {
                        ThrowingSpear throwingSpear = new ThrowingSpear();
                        throwingSpear.init(scanner);
                        equipment.add(throwingSpear);
                    }
                } catch (Exception e) {
                    log.error(e.getMessage());
                    i--;
                }
            }
        }
        return equipment;
    }

    public void findBySportType(List<SportsEquipment> equipment) throws InvalidInputException, NoEquipmentException {
        log.info("Выберите, инвентарь какого вида спорта вы хотели бы увидеть: (1 - Теннис, 2 - Тренажерный, 3 - Метательные виды спорта, 4 - Воллейбол): ");
        int type = scanner.nextInt();
        if (type <= 0 || type > 4) {
            throw new InvalidInputException("Не верный выбор вида спорта!");
        } else {
            switch (type) {
                case 1:
                    List<SportsEquipment> tennisEquipment = new ArrayList<SportsEquipment>();
                    for (SportsEquipment se : equipment) {
                        if (se.getSportType() == SportTypeEnum.TENNIS) {
                            tennisEquipment.add(se);
                        }
                    }
                    if (tennisEquipment.isEmpty()){
                        throw new NoEquipmentException("Инвентаря данного вида спорта на складе нет!");
                    }
                    log.info(tennisEquipment.toString());
                    break;
                case 2:
                    List<SportsEquipment> fitnessEquipment = new ArrayList<SportsEquipment>();
                    for (SportsEquipment fe : equipment) {
                        if (fe.getSportType() == SportTypeEnum.FITNESS) {
                            fitnessEquipment.add(fe);
                        }
                    }
                    if (fitnessEquipment.isEmpty()){
                        throw new NoEquipmentException("Инвентаря данного вида спорта на складе нет!");
                    }
                    log.info(fitnessEquipment.toString());
                    break;
                case 3:
                    List<SportsEquipment> throwingEquipment = new ArrayList<SportsEquipment>();
                    for (SportsEquipment te : equipment) {
                        if (te.getSportType() == SportTypeEnum.THROWING) {
                            throwingEquipment.add(te);
                        }
                    }
                    if (throwingEquipment.isEmpty()){
                        throw new NoEquipmentException("Инвентаря данного вида спорта на складе нет!");
                    }
                    log.info(throwingEquipment.toString());
                    break;
                case 4:
                    List<SportsEquipment> volleyballEquipment = new ArrayList<SportsEquipment>();
                    for (SportsEquipment ve : equipment) {
                        if (ve.getSportType() == SportTypeEnum.VOLLEYBALL) {
                            volleyballEquipment.add(ve);
                        }
                    }
                    if (volleyballEquipment.isEmpty()){
                        throw new NoEquipmentException("Инвентаря данного вида спорта на складе нет!");
                    }
                    log.info(volleyballEquipment.toString());
                    break;
            }
        }
    }
}

