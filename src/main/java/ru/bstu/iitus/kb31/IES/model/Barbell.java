package ru.bstu.iitus.kb31.IES.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import ru.bstu.iitus.kb31.IES.enums.SportTypeEnum;

import java.util.Scanner;

@EqualsAndHashCode(callSuper = true)
@Data
@Slf4j
public class Barbell extends Fitness {

    @Override
    public void init(Scanner scanner) {
        log.info("Введите вес штанги: ");
        weight = scanner.nextInt();
    }
    @Override
    public SportTypeEnum getSportType() {
        return SportTypeEnum.FITNESS;
    }

    @Override
    public String toString() {
        return "Штанга " +
                "весом: " + weight;
    }
}
