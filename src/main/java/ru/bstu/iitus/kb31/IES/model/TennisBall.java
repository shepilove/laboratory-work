package ru.bstu.iitus.kb31.IES.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import ru.bstu.iitus.kb31.IES.enums.SportTypeEnum;

import java.util.Scanner;

@EqualsAndHashCode(callSuper = true)
@Data
@Slf4j
public class TennisBall extends Ball {
    @Override
    public void init(Scanner scanner) {
        log.info("Введите радиус мяча:");
        radius = scanner.nextInt();
    }

    @Override
    public SportTypeEnum getSportType() {
        return SportTypeEnum.TENNIS;
    }

    @Override
    public String toString() {
        return "Тенисный мяч " +
                "радиусом " + radius;
    }
}

