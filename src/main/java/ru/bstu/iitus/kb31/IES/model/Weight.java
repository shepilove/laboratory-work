package ru.bstu.iitus.kb31.IES.model;

import lombok.extern.slf4j.Slf4j;
import ru.bstu.iitus.kb31.IES.enums.SportTypeEnum;

import java.util.Scanner;

@Slf4j
public class Weight extends Fitness {
    @Override
    public void init(Scanner scanner) {
        log.info("Введите вес гири:");
        weight = scanner.nextInt();
    }

    @Override
    public SportTypeEnum getSportType() {
        return SportTypeEnum.FITNESS;
    }

    @Override
    public String toString() {
        return "Гиря " +
                "весом: " + weight;
    }
}
