package ru.bstu.iitus.kb31.IES.enums;

public enum SportTypeEnum {
    TENNIS,
    VOLLEYBALL,
    FITNESS,
    THROWING,
}
