package ru.bstu.iitus.kb31.IES;

import lombok.extern.slf4j.Slf4j;
import ru.bstu.iitus.kb31.IES.exceptions.InvalidInputException;
import ru.bstu.iitus.kb31.IES.model.SportsEquipment;
import ru.bstu.iitus.kb31.IES.utility.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Slf4j
public class App {
    public static void main(String[] args) {
        boolean restartPut;
        boolean restartFind;

        Scanner scanner = new Scanner(System.in);
        Utility utility = new Utility();
        List<SportsEquipment> equipment = null;

        do {
            try {
                equipment = new ArrayList<SportsEquipment>(utility.putToCollection(scanner));
                restartPut = false;
            } catch (InvalidInputException e) {
                log.error(e.getMessage());
                restartPut = true;
            }
        } while (restartPut);

        do {
            try {
                utility.findBySportType(equipment);
                restartFind = false;
            } catch (Exception e) {
                log.error(e.getMessage());
                restartFind = true;
            }
        } while (restartFind);
    }
}