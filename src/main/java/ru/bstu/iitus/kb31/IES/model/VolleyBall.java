package ru.bstu.iitus.kb31.IES.model;

import lombok.extern.slf4j.Slf4j;
import ru.bstu.iitus.kb31.IES.enums.SportTypeEnum;

import java.util.Scanner;

@Slf4j
public class VolleyBall extends Ball {

    @Override
    public void init(Scanner scanner) {
        log.info("Введите радиус мяча:");
        radius = scanner.nextInt();
    }

    @Override
    public SportTypeEnum getSportType() {
        return SportTypeEnum.VOLLEYBALL;
    }

    @Override
    public String toString() {
        return "Волейбольный мяч " +
                "радиусом " + radius;
    }
}
