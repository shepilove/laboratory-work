package ru.bstu.iitus.kb31.IES.enums;

public enum EquipmentTypeEnum {
    BALL(1),
    BARBELL(7),
    FITNESS(3),
    RACQUET(2),
    TENNISBALL(5),
    TROWINGSPEAR(4),
    VOLLEYBALL(6),
    WEIGHT(8);

    private Integer key;

    EquipmentTypeEnum(Integer key) {
        this.key = key;
    }

    public Integer getKey() {
        return key;
    }
}
