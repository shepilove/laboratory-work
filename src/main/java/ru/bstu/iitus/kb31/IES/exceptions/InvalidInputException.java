package ru.bstu.iitus.kb31.IES.exceptions;

public class InvalidInputException extends Exception {
    public InvalidInputException(String message) {
        super(message);
    }
}
