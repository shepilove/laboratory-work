package ru.bstu.iitus.kb31.IES.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NoEquipmentException extends Exception {
    public NoEquipmentException(String message) {
        super(message);
    }
}
