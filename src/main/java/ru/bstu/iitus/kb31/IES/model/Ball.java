package ru.bstu.iitus.kb31.IES.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Scanner;

@EqualsAndHashCode(callSuper = true)
@Data
public class Ball extends SportsEquipment {
    int radius;

    @Override
    public void init(Scanner scanner) {
    }
}
